#  Template maven-publish

#  This template allows to test, build and publish with Maven your Java project.
#  The workflow allows running tests, code checkstyle and security scans on feature branches (as well as master).

# Prerequisites: pom.xml and appropriate project structure should exist in the repository.

# image: maven:3.6.3

image:
  name: surapanamasoa.azurecr.io/osb/soasuite-ci/12.2.1.3:latest
  username: $DOCKER_USERNAME
  password: $DOCKER_PASSWORD

# Workflow Configuration

pipelines:
  branches:
    feature/*:
      - parallel:
          - step:
              name: Test
              caches:
                - maven
              script:
                - mvn validate
              after-script:
                # Collect checkstyle check results if any and convert to Bitbucket Code Insights.
                - pipe: atlassian/checkstyle-report:0.2.0
          - step:
              name: Security Scan
              script:
                # Run a security scan for sensitive data.
                # See more security tools at https://bitbucket.org/product/features/pipelines/integrations?&category=security
                - pipe: atlassian/git-secrets-scan:0.4.3
    develop:
      - parallel:
          - step:
              name: Test
              caches:
                - maven
              script:
                - mvn validate
              after-script:
                # Collect checkstyle check results if any and convert to Bitbucket Code Insights.
                - pipe: atlassian/checkstyle-report:0.2.0
          - step:
              name: Security Scan
              script:
                # Run a security scan for sensitive data.
                # See more security tools at https://bitbucket.org/product/features/pipelines/integrations?&category=security
                - pipe: atlassian/git-secrets-scan:0.4.3
      - step:
          name: Build and test Develop
          max-time: 20
          caches:
            - maven
          script:
            - SERVICEOSB=$(mvn help:evaluate -Dexpression=osb.service -q -DforceStdout)
            - cd $SERVICEOSB
            # Bumps up the pom version
            - mvn release:prepare -DpreparationGoals=clean -Darguments="-Dmaven.deploy.skip=true" -DbranchName=${BITBUCKET_BRANCH} -DupdateBranchVersions=true -DupdateWorkingCopyVersions=true -DscmCommentPrefix="[skip ci] " # This bumps the versions in the poms, creates new commits, which will then get built by the master branch trigger.
            # Compile jar file
            # - mvn clean release:perform -DpreparationGoals=clean -Darguments="-Dmaven.deploy.skip=true" -DrepositoryId=internal -DpomFile=pom.xml -Durl=https://surapanama.jfrog.io/artifactory/sura-maven/
            - mvn package
            - cp -r ./target/ ../
            # Deploy files to artifactory
            - mvn deploy:deploy-file -Dfile='./target/${project.artifactId}-${project.version}.jar' -DrepositoryId=internal -DpomFile=pom.xml -Durl=https://surapanama.jfrog.io/artifactory/sura-maven/
          artifacts:
            - target/**
      - step:
          name: Deploy to Dev
          deployment: Test
          script:
            - SERVICEOSB=$(mvn help:evaluate -Dexpression=osb.service -q -DforceStdout)
            - cp -r ./target/ $SERVICEOSB/target
            - cd $SERVICEOSB
            - JARFILE=$(find ./target -type f ! -regex '.*/\([^/]*\)/\([^/]*\)/\1-\2[^/]*')
            - mvn com.oracle.servicebus.plugin:oracle-servicebus-plugin:deploy -DoracleServerUrl=${OSB_URLDEV} -DoracleUsername=${USER_OSBDEV} -DoraclePassword=${PASS_OSBDEV} -DconfigJar=${JARFILE} -DprojectName='${project.artifactId}'
            # ------ Deployment Using SSL ----- (It does not work until the certificates are valid)
            # - mvn com.oracle.servicebus.plugin:oracle-servicebus-plugin:deploy -X -DuseSSL=true -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -DoracleServerUrl=${OSB_URLDEV} -DoracleUsername=${USER_OSBDEV} -DoraclePassword=${PASS_OSBDEV} -DconfigJar=${JARFILE} -DprojectName='${project.artifactId}'
    release/*:
      - step:
          max-time: 20
          name: Build Staging
          caches:
            - maven
          script:
            - SERVICEOSB=$(mvn help:evaluate -Dexpression=osb.service -q -DforceStdout)
            - cd $SERVICEOSB
            - mvn versions:set -DremoveSnapshot
            - MVN_VER=$(cat pom.xml | grep "^    <version>.*</version>$" | awk -F'[><]' '{print $3}')
            - mvn versions:set -DnewVersion="${MVN_VER}-UAT"
            - mvn package
            - cp -r ./target/ ../
            - mvn deploy:deploy-file -Dfile='./target/${project.artifactId}-${project.version}.jar' -DrepositoryId=internal -DpomFile=pom.xml -Durl=https://surapanama.jfrog.io/artifactory/sura-maven/
          artifacts:
            - target/**
      - step:
          name: Deploy to UAT
          deployment: Staging
          trigger: manual
          script:
            - SERVICEOSB=$(mvn help:evaluate -Dexpression=osb.service -q -DforceStdout)
            - cp -r ./target/ $SERVICEOSB/target
            - cd $SERVICEOSB
            - JARFILE=$(find ./target -type f ! -regex '.*/\([^/]*\)/\([^/]*\)/\1-\2[^/]*')
            - mvn com.oracle.servicebus.plugin:oracle-servicebus-plugin:deploy -DoracleServerUrl=${OSB_URLUAT} -DoracleUsername=${USER_OSBUAT} -DoraclePassword=${PASS_OSBUAT} -DconfigJar=${JARFILE} -DprojectName='${project.artifactId}'
    master:
      - step:
          max-time: 20
          name: Build Staging
          caches:
            - maven
          script:
            - SERVICEOSB=$(mvn help:evaluate -Dexpression=osb.service -q -DforceStdout)
            - cd $SERVICEOSB
            - mvn versions:set -DremoveSnapshot
            - MVN_VER=$(cat pom.xml | grep "^    <version>.*</version>$" | awk -F'[><]' '{print $3}')
            - mvn versions:set -DnewVersion="${MVN_VER}-RELEASE"
            - mvn package
            - cp -r ./target/ ../
            - mvn deploy:deploy-file -Dfile='./target/${project.artifactId}-${project.version}.jar' -DrepositoryId=internal -DpomFile=pom.xml -Durl=https://surapanama.jfrog.io/artifactory/sura-maven/
          artifacts:
            - target/**
      # ------- Prod Deployment Stage ---------
      # - step:
      #     name: Deploy to Production
      #     deployment: Production
      #     trigger: manual
      #     script:
      #       - SERVICEOSB=$(mvn help:evaluate -Dexpression=osb.service -q -DforceStdout)
      #       - cp -r ./target/ $SERVICEOSB/target
      #       - cd $SERVICEOSB
      #       - JARFILE=$(find ./target -type f ! -regex '.*/\([^/]*\)/\([^/]*\)/\1-\2[^/]*')
      #       - mvn com.oracle.servicebus.plugin:oracle-servicebus-plugin:deploy -DoracleServerUrl=${OSB_URLPROD} -DoracleUsername=${USER_OSBPROD} -DoraclePassword=${PASS_OSBPROD} -DconfigJar=${JARFILE} -DprojectName='${project.artifactId}'
